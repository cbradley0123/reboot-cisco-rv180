# Reboot Cisco RV180

This python script is designed to log into a Cisco RV180 router and reboot it.  

The script was written in Python 3.4.2.  The script interacts via python Requests.  
To install the requests, the command is 'pip install requests'.

All you should have to do is put in the username, password, and IP of your Cisco RV180 router.
These variables are located immediately after the include statements in the script.

----- Licence ------

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses.