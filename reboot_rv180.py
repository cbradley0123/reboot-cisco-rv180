# ----- Licence ------

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see https://www.gnu.org/licenses.

# Author: Clayton Bradley
# Date: 9/16/2018
# Python version: 3.4.2

# Purpose: Log into cisco RV180 router and restart it.

# Note: The RV-180 has a two step reboot.  You call the reboot page, but then
#		trigger the reboot with a refresh call.

# Note 2: You will need to supply the username, password, and IP address of 
#		  your device before running this script.  These variables are located 
#		  immediately below the import statments.

import requests
import time

username = "<your device username>"
password = "<your device password>"
ip = '<your device IP address>'

print('----------------------------')
print('Starting script.')

#start the timer for execution
start_time = time.perf_counter()

# parameters sent when logging in.  To this will be added hidden variables.  
# deviceprofile and pageId, specifically.  They are found on the login page.
login_payload = {
	'users.username':	username,
	'users.password':	password,
	'button.login.users.home':	'Log+In',
	'Login.userAgent': 'Mozilla/5.0+(Windows+NT+10.0;+Win64;+x64;+rv:61.0)+Gecko/20100101+Firefox/61.0',
	'reload':	0,
	'thispage':	'index.htm'
}

# parameters needed to start the reboot
reboot_payload = {
	'button.reboot.statusPage': 'Reboot',
	'thispage':	'reboot.htm'
}

# when we hit the refresh page, which is actually the reboot start page
refresh_payload = {
	'button.redirect.statusPage2': '1',
	'redirect.ip':'',
	'thispage': 'statusPage.htm'
}

# by using session, we are going to retain cookies between requests
session_requests = requests.session()

# url for all requests
url = 'https://{}/platform.cgi'.format(ip)

# mimic Safari with these headers
user_agent = 'Mozilla/5.0+(Windows+NT+10.0;+Win64;+x64;+rv:61.0)+Gecko/20100101+Firefox/61.0'
accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
accept_encoding = 'gzip, deflate, br'
accept_language = 'en-US,en;q=0.5'

# send the login request using the login payload
result = session_requests.post(
	url, 
	headers = {'User-Agent': user_agent, 'Accept-Encoding': accept_encoding, 'Accept': accept, 'Accept-Language': accept_language, 'Upgrade-Insecure-Requests':'1', 'Host': ip, 'Referer': url, 'Connection': 'keep-alive'},
	data = login_payload,
	verify=False
)

# check for status code 200 which indicates a successful login, if we dont have it, log it and exit.
if result.status_code != 200:
	print('Failed to login.  Status code {} returned.\nExiting.'.format(result.status_code))
	exit()

print('Logged in.\n')

print("Sending reboot command\n")

# This is step 1 of 2 of the reboot.  
result = session_requests.post(
	url, 
	data = reboot_payload,
	headers = {'User-Agent': user_agent, 'Referer': '{}?page=reboot.htm'.format(url)}
)
print('Reboot command clicked.\n')
print ("Sending Refresh")

# This is step 2 of 2 of the reboot
result = session_requests.post(
	url, 
	data = refresh_payload,
	headers = {'User-Agent': user_agent, 'Referer': '{}?page=reboot.htm'.format(url)}
)
print("Refresh Sent.\n")
print('Finished.\n')
print ('Execution time: {} seconds.'.format(time.perf_counter() - start_time))